# SciFloat - Scientific Numbers

Enhance your numbers with automatic uncertainty propagation!

Tired of keeping track of your uncertainties, when calculating scientific formulae? Then this library is for you!


## Examples
Get uncertainties right in divisions, e.g. (2 +- 2) / (3 +- 3):
```
SciFloat(2, 2) / SciFloat(3, 3)  
# 0.6666666666666666 +- 0.9428090415820634
```

The objects can carry units and propagate them as well:
```
SciFloat(2, 2, unit='km') / SciFloat(3, 3, unit='h')  
# 0.6666666666666666 +- 0.9428090415820634 km/h
```

Decorating strings can be set globally:
```
SEP = '\pm'
SciFloat(2, 2)
# 2 \pm 2
```
More examples can be found in `examples`, e.g.
```
python examples/formulae/falling_object.py
```


## Installation
With pip or pipenv:
```
pipenv install "git+https://gitlab.com/andb0t/scifloat.git@master#egg=scifloat"
pip install "git+https://gitlab.com/andb0t/scifloat.git@master#egg=scifloat"
```

## Development
For testing the examples and running the code:
```
python setup.py develop  # locally build the library
python setup.py test  #run tests
```
For IPython use the config to preload the files on startup: `ipython --config ipython_config.py`


## Personal remark
Thanks for the interest in this library and for reading this far. Now, the author owes the reader a recommendation: use the [uncertainties](https://pythonhosted.org/uncertainties/) package instead of `scifloat`. It's ready to use, well tested and has been around for a while. `scifloat` is merely a hobby project.
