import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="scifloat",
    version="9.1.0",
    author="Andreas Maier",
    author_email="",
    description="Package to automatize uncertainty propagation",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/andb0t/scifloat",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        ],
    install_requires=[],
    setup_requires=["pytest-runner"],
    tests_require=["pytest"],
    )
