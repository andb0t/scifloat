"""Collection of tests for `pytest`."""
import math
# import random

# import pytest

from scifloat.scifloat import SciFloat


def test_acos():
    val = 0
    assert math.acos(val) == math.acos(SciFloat(val)).val


def test_acosh():
    val = 1
    assert math.acosh(val) == math.acosh(SciFloat(val)).val


def test_asin():
    val = 0
    assert math.asin(val) == math.asin(SciFloat(val)).val


def test_asinh():
    val = 0
    assert math.asinh(val) == math.asinh(SciFloat(val)).val


def test_atan():
    val = 0
    assert math.atan(val) == math.atan(SciFloat(val)).val


def test_atan2():
    val = (1, 1)
    assert math.atan2(*val) == math.atan2(SciFloat(val[0]), SciFloat(val[1])).val


def test_atanh():
    val = 0
    assert math.atanh(val) == math.atanh(SciFloat(val)).val


def test_ceil():
    val = 0
    assert math.ceil(val) == math.ceil(SciFloat(val)).val


def test_copysign():
    val = (0, 0)
    assert math.copysign(*val) == math.copysign(SciFloat(val[0]), SciFloat(val[1])).val


def test_cos():
    val = 0
    assert math.cos(val) == math.cos(SciFloat(val)).val


def test_cosh():
    val = 0
    assert math.cosh(val) == math.cosh(SciFloat(val)).val


def test_degrees():
    val = 0
    assert math.degrees(val) == math.degrees(SciFloat(val)).val


def test_erf():
    val = 0
    assert math.erf(val) == math.erf(SciFloat(val)).val


def test_erfc():
    val = 0
    assert math.erfc(val) == math.erfc(SciFloat(val)).val


def test_exp():
    val = 0
    assert math.exp(val) == math.exp(SciFloat(val)).val


def test_expm1():
    val = 0
    assert math.expm1(val) == math.expm1(SciFloat(val)).val


def test_fabs():
    val = 0
    assert math.fabs(val) == math.fabs(SciFloat(val)).val


def test_factorial():
    val = 0
    assert math.factorial(val) == math.factorial(SciFloat(val)).val


def test_floor():
    val = 0
    assert math.floor(val) == math.floor(SciFloat(val)).val


def test_fmod():
    val = (0, 1)
    assert math.fmod(*val) == math.fmod(SciFloat(val[0]), SciFloat(val[1])).val


def test_frexp():
    val = 0.5
    assert math.frexp(val) == math.frexp(SciFloat(val)).val


def test_fsum():
    val = [0, 1, 2]
    scifloat_val = [SciFloat(val[0]), SciFloat(val[1]), SciFloat(val[2])]
    assert math.fsum(val) == math.fsum(scifloat_val).val


def test_gamma():
    val = 1
    assert math.gamma(val) == math.gamma(SciFloat(val)).val


def test_gcd():
    val = (3, 15)
    assert math.gcd(*val) == math.gcd(SciFloat(val[0]), SciFloat(val[1])).val


def test_hypot():
    val = (3, 4)
    assert math.hypot(*val) == math.hypot(SciFloat(val[0]), SciFloat(val[1])).val


def test_isclose():
    val = (0, 0)
    assert math.isclose(*val) == math.isclose(SciFloat(val[0]), SciFloat(val[1])).val


def test_isfinite():
    val = 0
    assert math.isfinite(val) == math.isfinite(SciFloat(val)).val


def test_isinf():
    val = 0
    assert math.isinf(val) == math.isinf(SciFloat(val)).val


def test_isnan():
    val = 0
    assert math.isnan(val) == math.isnan(SciFloat(val)).val


def test_ldexp():
    val = (2, 3)
    assert math.ldexp(*val) == math.ldexp(SciFloat(val[0]), SciFloat(val[1])).val


def test_lgamma():
    val = 1
    assert math.lgamma(val) == math.lgamma(SciFloat(val)).val


def test_log():
    val = 1
    assert math.log(val) == math.log(SciFloat(val)).val


def test_log10():
    val = 1
    assert math.log10(val) == math.log10(SciFloat(val)).val


def test_log1p():
    val = 1
    assert math.log1p(val) == math.log1p(SciFloat(val)).val


def test_log2():
    val = 1
    assert math.log2(val) == math.log2(SciFloat(val)).val


def test_modf():
    val = 0
    assert math.modf(val) == math.modf(SciFloat(val)).val


def test_pow():
    val = (2, 3)
    assert math.pow(*val) == math.pow(SciFloat(val[0]), SciFloat(val[1])).val


def test_radians():
    val = 0
    assert math.radians(val) == math.radians(SciFloat(val)).val


def test_sin():
    val = 0
    assert math.sin(val) == math.sin(SciFloat(val)).val


def test_sinh():
    val = 0
    assert math.sinh(val) == math.sinh(SciFloat(val)).val


def test_sqrt():
    val = 0
    assert math.sqrt(val) == math.sqrt(SciFloat(val)).val


def test_tan():
    val = 0
    assert math.tan(val) == math.tan(SciFloat(val)).val


def test_tanh():
    val = 0
    assert math.tanh(val) == math.tanh(SciFloat(val)).val


def test_trunc():
    val = 0
    assert math.trunc(val) == math.trunc(SciFloat(val)).val
