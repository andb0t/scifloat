import math
import functools

import sympy

from scifloat.scifloat import SciFloat


def scifloatify_sympy(func):
    """Wrap functions to allow SciFloat arguments."""
    @functools.wraps(func)
    def wrapper(*args):
        try:
            # for a single float argument
            return func(*args)
        except (AttributeError, sympy.SympifyError):
            # for one or more "SciFloat" arguments
            arguments = map(lambda x: x.val, args)
            return SciFloat(func(*arguments))
    return wrapper


sympy.acos = scifloatify_sympy(sympy.acos)
sympy.acosh = scifloatify_sympy(sympy.acosh)
sympy.asin = scifloatify_sympy(sympy.asin)
sympy.asinh = scifloatify_sympy(sympy.asinh)
sympy.atan = scifloatify_sympy(sympy.atan)
sympy.atan2 = scifloatify_sympy(sympy.atan2)
sympy.atanh = scifloatify_sympy(sympy.atanh)
sympy.ceiling = scifloatify_sympy(sympy.ceiling)
sympy.cos = scifloatify_sympy(sympy.cos)
sympy.cosh = scifloatify_sympy(sympy.cosh)
sympy.erf = scifloatify_sympy(sympy.erf)
sympy.erfc = scifloatify_sympy(sympy.erfc)
sympy.exp = scifloatify_sympy(sympy.exp)
sympy.factorial = scifloatify_sympy(sympy.factorial)
sympy.floor = scifloatify_sympy(sympy.floor)
sympy.gamma = scifloatify_sympy(sympy.gamma)
sympy.gcd = scifloatify_sympy(sympy.gcd)
sympy.log = scifloatify_sympy(sympy.log)
sympy.Pow = scifloatify_sympy(sympy.Pow)
sympy.sin = scifloatify_sympy(sympy.sin)
sympy.sinh = scifloatify_sympy(sympy.sinh)
sympy.sqrt = scifloatify_sympy(sympy.sqrt)
sympy.tan = scifloatify_sympy(sympy.tan)
sympy.tanh = scifloatify_sympy(sympy.tanh)


def scifloatify_math(func):
    """Wrap functions to allow SciFloat arguments."""
    @functools.wraps(func)
    def wrapper(*args):
        try:
            # for a single float argument
            return func(*args)
        except (TypeError, SystemError):
            try:
                # for one or more "SciFloat" arguments
                arguments = map(lambda x: x.val, args)
                return SciFloat(func(*arguments))
            except AttributeError:
                # for a single "List of SciFloats" argument
                argument = map(lambda x: x.val, args[0])
                return SciFloat(func(argument))
    return wrapper


math.acos = scifloatify_math(math.acos)
math.acosh = scifloatify_math(math.acosh)
math.asin = scifloatify_math(math.asin)
math.asinh = scifloatify_math(math.asinh)
math.atan = scifloatify_math(math.atan)
math.atan2 = scifloatify_math(math.atan2)
math.atanh = scifloatify_math(math.atanh)
math.ceil = scifloatify_math(math.ceil)
math.copysign = scifloatify_math(math.copysign)
math.cos = scifloatify_math(math.cos)
math.cosh = scifloatify_math(math.cosh)
math.degrees = scifloatify_math(math.degrees)
math.e = scifloatify_math(math.e)
math.erf = scifloatify_math(math.erf)
math.erfc = scifloatify_math(math.erfc)
math.exp = scifloatify_math(math.exp)
math.expm1 = scifloatify_math(math.expm1)
math.fabs = scifloatify_math(math.fabs)
math.factorial = scifloatify_math(math.factorial)
math.floor = scifloatify_math(math.floor)
math.fmod = scifloatify_math(math.fmod)
math.frexp = scifloatify_math(math.frexp)
math.fsum = scifloatify_math(math.fsum)
math.gamma = scifloatify_math(math.gamma)
math.gcd = scifloatify_math(math.gcd)
math.hypot = scifloatify_math(math.hypot)
math.inf = scifloatify_math(math.inf)
math.isclose = scifloatify_math(math.isclose)
math.isfinite = scifloatify_math(math.isfinite)
math.isinf = scifloatify_math(math.isinf)
math.isnan = scifloatify_math(math.isnan)
math.ldexp = scifloatify_math(math.ldexp)
math.lgamma = scifloatify_math(math.lgamma)
math.log = scifloatify_math(math.log)
math.log10 = scifloatify_math(math.log10)
math.log1p = scifloatify_math(math.log1p)
math.log2 = scifloatify_math(math.log2)
math.modf = scifloatify_math(math.modf)
math.nan = scifloatify_math(math.nan)
math.pi = scifloatify_math(math.pi)
math.pow = scifloatify_math(math.pow)
math.radians = scifloatify_math(math.radians)
math.sin = scifloatify_math(math.sin)
math.sinh = scifloatify_math(math.sinh)
math.sqrt = scifloatify_math(math.sqrt)
math.tan = scifloatify_math(math.tan)
math.tanh = scifloatify_math(math.tanh)
math.tau = scifloatify_math(math.tau)
math.trunc = scifloatify_math(math.trunc)
