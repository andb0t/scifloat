"""Collection of tests for `pytest`."""
import pytest

from scifloat.units import parse, reduce, resolve_powers


def test_reduce():
    assert reduce('km') == 'km'
    assert reduce('km/h') == 'km/h'
    assert reduce('km/h*h') == 'km'
    assert reduce('1/km') == '1/km'
    assert reduce('/km') == '1/km'
    assert reduce('1/km*km') == ''
    assert reduce('1/km/km') == '1/km^2'
    assert reduce('km*km') == 'km^2'
    assert reduce('K*K*K/K/K*h*h') == 'K*h^2'


def test_parse():
    assert parse('km/h') == ['*km', '/h']
    assert parse('*km/h') == ['*km', '/h']
    assert parse('') == []
    # powers
    assert parse('km^2') == ['*km', '*km']
    assert parse('1/km^3') == ['*1', '/km', '/km', '/km']
    assert parse('h^2/km^3/h') == ['*h', '*h', '/km', '/km', '/km', '/h']
    # nonsense
    with pytest.raises(NotImplementedError):
        _ = parse('***km///h')
    with pytest.raises(NotImplementedError):
        _ = parse('**//**')


def test_resolve_powers():
    assert resolve_powers(['*km^2']) == ['*km', '*km']
    assert resolve_powers(['*h^3', '/km^2']) == ['*h', '*h', '*h', '/km', '/km']
