"""Collection of tests for `pytest`."""
import sympy
# import random

# import pytest

from scifloat.scifloat import SciFloat


def test_acos():
    val = 0
    assert sympy.acos(val) == sympy.acos(SciFloat(val)).val


def test_acosh():
    val = 1
    assert sympy.acosh(val) == sympy.acosh(SciFloat(val)).val


def test_asin():
    val = 0
    assert sympy.asin(val) == sympy.asin(SciFloat(val)).val


def test_asinh():
    val = 0
    assert sympy.asinh(val) == sympy.asinh(SciFloat(val)).val


def test_atan():
    val = 0
    assert sympy.atan(val) == sympy.atan(SciFloat(val)).val


def test_atan2():
    val = (1, 1)
    assert sympy.atan2(*val) == sympy.atan2(SciFloat(val[0]), SciFloat(val[1])).val


def test_atanh():
    val = 0
    assert sympy.atanh(val) == sympy.atanh(SciFloat(val)).val


def test_ceiling():
    val = 0
    assert sympy.ceiling(val) == sympy.ceiling(SciFloat(val)).val


def test_cos():
    val = 0
    assert sympy.cos(val) == sympy.cos(SciFloat(val)).val


def test_cosh():
    val = 0
    assert sympy.cosh(val) == sympy.cosh(SciFloat(val)).val


def test_erf():
    val = 0
    assert sympy.erf(val) == sympy.erf(SciFloat(val)).val


def test_erfc():
    val = 0
    assert sympy.erfc(val) == sympy.erfc(SciFloat(val)).val


def test_exp():
    val = 0
    assert sympy.exp(val) == sympy.exp(SciFloat(val)).val


def test_factorial():
    val = 0
    assert sympy.factorial(val) == sympy.factorial(SciFloat(val)).val


def test_floor():
    val = 0
    assert sympy.floor(val) == sympy.floor(SciFloat(val)).val


def test_gamma():
    val = 1
    assert sympy.gamma(val) == sympy.gamma(SciFloat(val)).val


def test_gcd():
    val = (3, 15)
    assert sympy.gcd(*val) == sympy.gcd(SciFloat(val[0]), SciFloat(val[1])).val


def test_log():
    val = 1
    assert sympy.log(val) == sympy.log(SciFloat(val)).val


def test_Pow():
    val = (2, 3)
    assert sympy.Pow(*val) == sympy.Pow(SciFloat(val[0]), SciFloat(val[1])).val


def test_sin():
    val = 0
    assert sympy.sin(val) == sympy.sin(SciFloat(val)).val


def test_sinh():
    val = 0
    assert sympy.sinh(val) == sympy.sinh(SciFloat(val)).val


def test_sqrt():
    val = 0
    assert sympy.sqrt(val) == sympy.sqrt(SciFloat(val)).val


def test_tan():
    val = 0
    assert sympy.tan(val) == sympy.tan(SciFloat(val)).val


def test_tanh():
    val = 0
    assert sympy.tanh(val) == sympy.tanh(SciFloat(val)).val
