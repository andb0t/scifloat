"""Collection of tests for `pytest`."""
import math
import random

import pytest

from scifloat.scifloat import SciFloat


def rand(pos=False, rng=1e6):
    if pos:
        return random.uniform(0, rng)
    return random.uniform(-rng, rng)


def test_components():
    assert SciFloat(2, 1).val == 2
    assert SciFloat(2, 1).unc == 1
    assert SciFloat(2).unc == 0
    # exceptions
    with pytest.raises(AssertionError):
        _ = SciFloat(2, -1)


def test_equality():
    assert SciFloat(2, 1, unit='') == SciFloat(2, 1)
    assert SciFloat(2, 1, unit='kg') != SciFloat(2, 1)
    assert SciFloat(2, 1, sep='±') == SciFloat(2, 1)
    assert SciFloat(2, 1, sep='\\pm') != SciFloat(2, 1)
    assert SciFloat(1) == 1
    assert SciFloat(1, unit='km') != 1


def test_addition():
    # SciFloat +- SciFloat
    assert SciFloat(2, 2) + SciFloat(1, 1) == SciFloat(3, math.sqrt(5))
    assert SciFloat(2, 2) - SciFloat(1, 1) == SciFloat(1, math.sqrt(5))
    # SciFloat +- scalar
    assert SciFloat(2, 2) - 1.5 == SciFloat(0.5, 2)
    assert 1.5 - SciFloat(2, 2) == - SciFloat(0.5, 2)
    # exceptions
    with pytest.raises(TypeError):
        _ = SciFloat(2, 2, unit='kg') - 2


def test_multiplication():
    # SciFloat * SciFloat
    assert SciFloat(2, 2) * SciFloat(1, 1) == SciFloat(2, math.sqrt(8))
    # SciFloat * scalar
    assert SciFloat(2, 2) * 1 == SciFloat(2, 2)
    assert SciFloat(2, 2) * 0 == SciFloat(0, 0)
    assert SciFloat(2, 2) * 5 == SciFloat(10, 10)
    assert 5 * SciFloat(2, 2) == SciFloat(10, 10)

    def mult(a, da, b, db):
        return math.sqrt(pow(da * b, 2) + pow(a * db, 2))

    for _ in range(100):
        a = rand()
        da = rand(True)
        b = rand()
        db = rand(True)
        assert SciFloat(a, da) * SciFloat(b, db) == SciFloat(a * b, mult(a, da, b, db))


def test_division():
    # SciFloat / SciFloat
    assert SciFloat(2, 2) / SciFloat(1, 1) == SciFloat(2, 2.8284271247461903)
    assert SciFloat(2, 2) / SciFloat(2, 1) == SciFloat(1, 1.118033988749895)
    assert SciFloat(2, 2) / SciFloat(5, 3) == SciFloat(0.4, 0.46647615158762407)
    # exceptions
    with pytest.raises(ZeroDivisionError):
        _ = SciFloat(2, 2) / SciFloat(0, 4)
    # SciFloat / scalar
    assert SciFloat(2, 2) / 0.5 == SciFloat(4, 4)
    assert 10 / SciFloat(2, 2) == SciFloat(5, 5)
    assert 10 / SciFloat(2, 5) == SciFloat(5, 12.5)
    # exceptions
    with pytest.raises(ZeroDivisionError):
        _ = SciFloat(2, 2) / 0

    def nazzan(a, da, b, db):
        return math.sqrt(pow(a, 2) * pow(db, 2) / pow(b, 4) + pow(da, 2) / pow(b, 2))

    for _ in range(100):
        a = rand()
        da = rand(True)
        b = rand()
        db = rand(True)
        assert SciFloat(a, da) / SciFloat(b, db) == SciFloat(a / b, nazzan(a, da, b, db))


def test_exponents():
    assert pow(SciFloat(2, 2), 2) == SciFloat(4, 8)
    assert pow(SciFloat(2, 2), 3) == SciFloat(8, 24)
    assert pow(SciFloat(-2, 2), 3) == SciFloat(-8, 24)
    assert pow(pow(SciFloat(2, 2), 2), 0.5) == SciFloat(2, 2)
    for _ in range(100):
        a = rand(True, rng=1e3)
        da = rand(True, rng=1e3)
        b = rand(rng=10)
        assert pow(pow(SciFloat(a, da), b), 1/b) == SciFloat(a, da)


def test_units():
    # single unit reduction
    assert (SciFloat(2, unit='km')).unit == 'km'
    assert (SciFloat(2, unit='1/K')).unit == '1/K'
    assert (SciFloat(2, unit='K/K')).unit == ''
    assert (SciFloat(2, unit='km/h/m/h*h')).unit == 'km/h/m'
    assert (SciFloat(2, unit='km/h/m/h*h')).unit == 'km/h/m'
    # multiplication and division of SciFloat
    assert (SciFloat(2, unit='km') / SciFloat(2, unit='h')).unit == 'km/h'
    assert (SciFloat(2, unit='km') / SciFloat(2, unit='h') * SciFloat(2, unit='h')).unit == 'km'
    assert (SciFloat(2, unit='km/h') * SciFloat(2, unit='h')).unit == 'km'
    assert (SciFloat(2, unit='km/h') / SciFloat(2, unit='a')).unit == 'km/h/a'
    # multiplication and division of SciFloat with scalar
    assert (2 * SciFloat(2, unit='km*h')).unit == 'km*h'
    assert (SciFloat(2, unit='km*h') / 2).unit == 'km*h'
    assert (2 / SciFloat(2, unit='km*h')).unit == '1/km*h'
    # equality
    assert SciFloat(2, unit='km*h') == SciFloat(2, unit='h*km')
    assert SciFloat(2, unit='') == SciFloat(2, unit='1/h*h')
    # powers of units
    assert SciFloat(2, unit='h*h') == SciFloat(2, unit='h^2')
    assert SciFloat(2, unit='1/h/h') == SciFloat(2, unit='1/h^2')
    assert SciFloat(2, unit='km/h/h*km') == SciFloat(2, unit='km^2/h^2')
    # powers of SciFloats
    assert pow(SciFloat(2, unit='s'), 2) == SciFloat(4, unit='s^2')


def test_fractional_units():
    assert pow(SciFloat(4, unit='s'), 0.5) == SciFloat(2, unit='s^0.5')
    assert pow(SciFloat(4, unit='s'), -0.5) == SciFloat(0.5, unit='s^-0.5')
    # assert pow(SciFloat(4, unit='s^2'), 0.5) == SciFloat(2, unit='s')  # this fails!


def test_relative_uncertainty():
    assert SciFloat(1, 1).runc == 1
    assert math.isclose(SciFloat(220, 2.4, unit='V').runc, 0.010909090909090908)
    assert math.isclose(SciFloat(220, runc=0.010909090909090908).runc, 0.010909090909090908)
    assert SciFloat(220, runc=0.010909090909090908) == SciFloat(220, 2.4)
    assert SciFloat(0, 2).runc is math.nan
