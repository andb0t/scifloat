"""A package to automatize uncertainty propagation."""
name = "scifloat"

import math

import scifloat.extension_math
from scifloat.scifloat import SciFloat
