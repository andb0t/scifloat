"""A number class equipped with uncertainties."""
import math

from . import units


SEP = '±'
UNIT = ''


class SciFloat:
    """A number with an uncertainty

    Keyword arguments:
    value       -- the central value of the number
    unc         -- its uncertainty, use 'stat' for automatic assignment to sqrt(value)
    sep         -- the separator between value and uncertainty for printing
    unit        -- the unit of the number
    runc        -- relative uncertainty

    """
    def __init__(self, value, unc=None, sep=None, unit=None, runc=None):  # pylint: disable=R0913
        self.val = value
        if unc is not None and runc is not None:
            raise Exception('cannot pass both absolute and relative uncertainty')
        elif unc is not None:
            self.unc = unc
            try:
                self.runc = self.unc / self.val
            except ZeroDivisionError:
                self.runc = math.nan
        elif runc is not None:
            self.runc = runc
            self.unc = self.val * self.runc
        else:
            self.unc = self.runc = 0
        assert self.unc >= 0, "uncertainty cannot be negative"
        self.sep = sep if sep is not None else SEP
        self.unit = units.reduce(unit) if unit is not None else UNIT

    def __str__(self):
        return '{} {} {}{}{}'.format(self.val, self.sep, self.unc, ' ' if self.unit else '', self.unit)

    def __repr__(self):
        return '(' + self.__str__() + ')'

    def __format__(self, format_spec):
        if self.unc:
            return format(self.val, format_spec) + ' {} '.format(self.sep) + \
                format(self.unc, format_spec) + (' ' if self.unit else '') + self.unit
        return format(self.val, format_spec) + (' ' if self.unit else '') + self.unit

    def __eq__(self, other):
        try:
            return (
                math.isclose(self.val, other.val) and
                math.isclose(self.unc, other.unc) and
                self.sep == other.sep and
                units.equal(self.unit, other.unit)
                )
        except AttributeError:
            return (
                math.isclose(self.val, other) and
                math.isclose(self.unc, 0) and
                self.unit == ''
                )

    def __neg__(self):
        return SciFloat(-self.val, self.unc, sep=self.sep, unit=self.unit)

    def __add__(self, other):
        try:
            if not units.equal(self.unit, other.unit):
                raise TypeError('cannot add numbers of different units ({} != {})'.format(self.unit, other.unit))
            value = self.val + other.val
            unc = math.sqrt(self.unc ** 2 + other.unc ** 2)
        except AttributeError:
            if self.unit:
                raise TypeError('cannot add number with unit to scalar')
            value = self.val + other
            unc = self.unc
        return SciFloat(value, unc, sep=self.sep, unit=self.unit)

    def __sub__(self, other):
        return self + -other

    def __mul__(self, other):
        try:
            value = self.val * other.val
            unc = math.sqrt((other.unc * self.val) ** 2 +
                            (self.unc * other.val) ** 2)
            unit = units.reduce(self.unit + '*' + other.unit)
        except AttributeError:
            value = self.val * other
            unc = self.unc * other
            unit = self.unit
        return SciFloat(value, unc, sep=self.sep, unit=unit)

    def __pow__(self, other):
        return SciFloat(math.pow(self.val, other),
                        math.fabs(self.unc * other * math.pow(self.val, other-1)),
                        unit=self.unit + '^' + str(other) if self.unit else '',
                        sep=self.sep
                        )

    def __truediv__(self, other):
        try:
            value = self.val / other.val
            unc = math.sqrt(
                (self.unc / other.val) ** 2 +
                (other.unc * self.val / other.val ** 2) ** 2
                )
            unit = units.reduce(self.unit + '/' + other.unit)
        except AttributeError:
            value = self.val / other
            unc = self.unc / other
            unit = self.unit
        return SciFloat(value, unc, sep=self.sep, unit=unit)

    def __rtruediv__(self, other):
        try:
            value = other.val / self.val
            unc = math.sqrt((other.unc / self.val) ** 2 +
                            (self.unc * other.val / self.val ** 2) ** 2)
            unit = units.reduce(other.unit + '/' + self.unit)
        except AttributeError:
            value = other / self.val
            unc = self.unc * other / self.val ** 2
            unit = ('1/' + self.unit) if self.unit else ''
        return SciFloat(value, unc, sep=self.sep, unit=unit)

    def __radd__(self, other):
        return self + other

    def __rsub__(self, other):
        return other + -self

    def __rmul__(self, other):
        return self * other

    def __getitem__(self, i):
        if i == 0:
            return self.val
        if i == 1:
            return self.unc
        raise IndexError('indices > 1 not implemented!')
