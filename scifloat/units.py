"""Functions to handle units."""
from collections import Counter
import re


def resolve_powers(units):
    """Resolve powers of units: ['*h^2'] -> ['*h', '*h']"""
    new_units = []
    for unit in units:
        if '^' not in unit:
            new_units.append(unit)
        else:
            power = int(re.findall(r"\d+", unit)[0])
            resolved_units = [unit.split('^')[0]] * power
            new_units.extend(resolved_units)
    return new_units


def parse(unit):
    """Split into list of single units."""
    unit = unit.replace(' ', '')
    if any(double_op in unit for double_op in ['**', '*/', '//', '/*']):
        raise NotImplementedError('back-t0=o-back operators in units are not implemented')
    if unit and not unit.startswith('*'):
        unit = '*' + unit
    for op in ['*', '/']:
        unit = unit.replace(op, ' ' + op)
    units = [i.strip() for i in unit.split() if i]
    return resolve_powers(units)


def reduce(unit):
    """Keep track of units and cancel them out."""

    def walk(tmp, result=''):
        if not tmp:
            return result
        elt = tmp[0][1:]  # returns the first unit without * or /
        pos = tmp.count('*' + elt)
        neg = tmp.count('/' + elt)
        tot = pos - neg
        if tot > 1:
            result = result + '*' + elt + '^' + str(tot)
        elif tot == 1:
            result = result + '*' + '*'.join([elt] * tot)
        elif tot == -1:
            result = result + '/' + '/'.join([elt] * (-tot))
        elif tot < -1:
            result = result + '/' + elt + '^' + str(-tot)
        result = result.strip('*')
        if result.startswith('/'):
            result = '1' + result
        if result == '1':
            result = ''
        tmp = list(filter(lambda x: x not in ['*' + elt, '/' + elt], tmp))
        return walk(tmp, result)

    return walk(parse(unit))


def equal(unit, other):
    """Evaluate equality of units."""
    return Counter(parse(unit)) == Counter(parse(other))
