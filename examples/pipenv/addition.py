"""A simple example to show SciFloat additions."""
from scifloat import SciFloat

a = SciFloat(3, 3)
b = SciFloat(2, 2)

print("Add", a, 'and', b, ' ...')

result = a + b

print('The result is:', result)
