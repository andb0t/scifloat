"""Calculate total length of a sum of measurements."""
from scifloat import SciFloat

l1 = SciFloat(0.25, unc=0.005, unit='m')
l2 = SciFloat(0.30, unc=0.005, unit='m')
ltot = l1 + l2
print('The total length of', l1, 'and', l2, 'is:', ltot)
