"""Calculate distance travelled by a falling object."""
from scifloat import SciFloat

g = SciFloat(9.81, unit='N/kg')
t = SciFloat(1, runc=0.09, unit='s')
d = g / 2 * pow(t, 2)
print('The travelled distance of an object falling for ', t, 'seconds is:', d)
