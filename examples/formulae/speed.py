"""Calculate speed from measurement of distance and time."""
from scifloat import SciFloat

d = SciFloat(3, runc=0.03, unit='m')
t = SciFloat(1, runc=0.05, unit='s')
speed = d / t
print('The speed from distance', d, 'and time', t, 'is:', speed)
